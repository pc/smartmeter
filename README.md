# nodeMCU based smart meter reader

Simple piggy-back smart meter reader that will count power impulses using a light sensor and a nodeMCU in real time and push calculated values via WiFi/MQTT to a local MQTT broker (in my case a [Home Assistant | https://home-assistant.io] device).

It consist of three parts:

* nodeMCU circuit build on a breadboard
* Sketch for reading, counting and communicating via MQTT
* Home Assistant configuration (sensors) to display and store that data

## nodeMCU circuit

All you need:

* nodeMCU bread
* Light sensor
* 1k resistor
* few jumper wires

Connections:
* 3V to one leg of the light sensor (1st leg)
* GND over the 1k resistor to the other leg of the light sensor (2nd leg)
* ADC0 directly to the 2nd leg.

## Sketch

In order to use MQTT, WiFi etc, there are some libraries  needed:  **PubSubClient**, **ArduinoJson**, **TM1637Display** (in my case for the 4-digit LED display). You can find the sketch in this project.

## Home Assistant MQTT Sensor Configuration (optional)

As a prerequirement, Home Assistant needs to be setup to support MQTT. You'll find more information at [Home Assistant MQTT component page | https://home-assistant.io/components/mqtt/].

### Sensors

The first sensor simply gets the calculated value that was pushed by the sensor and can be displayed by HASS.

```
  - platform: mqtt
    state_topic: 'house/power_consumption'
    name: 'Home Power Consumption'
    unit_of_measurement: 'Wh'
    value_template: '{{ value_json.watt }}'
```

This second sensor will publish the counted ticks coming from your power meter. Since my power meter has 500 impulses per kW, I'll get the kW-value by dividing the counter with 500. This is highly depending on you power meter. Therefore **you'll need to change it**.

```
  - platform: mqtt
    state_topic: 'house/power_consumption'
    name: 'Home Power Consumed'
    value_template: "{{ '%.1f' | format(value_json.impulses | float / 500)  }}"
    unit_of_measurement: "kWh"
```

The data is not persisted on the nodeMCU. Therefore you'll need to initially setup the counter for the second sensor to the actual power meter value by publishig a MQTT message with the topic **house/set_count_impulses** and in my case actual kWh times 500 (impulses) e.g. for 2000 kWh its 1000000.

# 4-Digit LED display

With a TM1637 display connected to the nodeMCU (CLK = D2, DIO = D3), you'll also get real time power consumption displayed directly on the small display. I've used it for debugging only.

# Installation

Just mount the device on your power meter using duct tape in a way that the light sensor is directly placed over the impulse LED of the power meter.

Don't forgett to set your WiFi and MQTT credentails! Also check your power meter and set **WATT_HOURS_PER_IMPULSE** to the proper value (in my case it is 2Wh since my power meter has 500 impulses per 1kW).
