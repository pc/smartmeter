#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <TM1637Display.h>
#include <ArduinoJson.h>

//
#define lightsensor A0
#define MQTT_VERSION MQTT_VERSION_3_1_1
#define LED D0

// Wifi: SSID and password
const PROGMEM char*     WIFI_SSID = "<your ssid>";
const PROGMEM char*     WIFI_PASSWORD = "<your wifi password>";

// MQTT: ID, server IP, port, username and password
const PROGMEM char*     MQTT_CLIENT_ID = "<client id>";
const PROGMEM char*     MQTT_SERVER_IP = "<your mqtt broker>";
const PROGMEM uint16_t  MQTT_SERVER_PORT = 1883;
const PROGMEM char*     MQTT_USER = "<your mqtt user";
const PROGMEM char*     MQTT_PASSWORD = "<your mqtt password";

// MQTT: topic
const PROGMEM char*     MQTT_SENSOR_TOPIC = "house/power_consumption";
const PROGMEM char*     MQTT_SUBSCIBTION_TOPIC ="house/set_count_impulses";
//const PROGMEM char*     MQTT_SUBSCIBTION_TOPIC ="foobar";

// for the 4 digit lcd display
const int  CLK = D2;
const int  DIO = D3;

TM1637Display display(CLK, DIO);

WiFiClient wifiClient;
PubSubClient client(wifiClient);

const int   THREHOLD                = 90; // light threhold. If lower, don't count
const int   THREHOLD_MS_POWER       = 500; // min threhold between two impulses
const int   WATT_HOURS_PER_IMPULSE  = 2; // 1kWh = 500 impulses

double  last_watt             = 0;
double  unsent_last_watt      = -1;

long  last_impulse_duration   = 0;
long  impulse_pending_since   = 0;
long  lastPulse               = 0; //timestamp
long  lastReconnectAttempt    = 0; //timestamp
long  ticks_counter           = 0;



// MQTT PART

// function called to publish power consumption
void publishData(long watt) {
  // create a JSON object
  // doc : https://github.com/bblanchon/ArduinoJson/wiki/API%20Reference
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();

  root["watt"] = (String) watt;
  root["impulses"] = (String) ticks_counter;
  root.prettyPrintTo(Serial);
  Serial.println("");
  /*
     {
        "watt": "1234" ,
        "impulses": "123",
     }
  */
  char data[200];
  root.printTo(data, root.measureLength() + 1);
  client.publish(MQTT_SENSOR_TOPIC, data, true);
}

boolean reconnect() {
  if (client.connect(MQTT_CLIENT_ID, MQTT_USER, MQTT_PASSWORD)) {
    display.showNumberDec((int) 030, false, 4,0);
    client.subscribe(MQTT_SUBSCIBTION_TOPIC);
    Serial.println("INFO: connected");
  } else {
    Serial.print("ERROR: failed, rc=");
    Serial.println(client.state());
  }
  return client.connected();
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message received [");
  Serial.print(topic);
  Serial.print("] ");
  char message_buff[100];
  int i = 0;
  for ( i = 0; i < length; i++) {
    message_buff[i] = payload[i];
  }
  message_buff[length] = '\0';
  Serial.println(message_buff);
  set_ticks_counter(atol(message_buff));
}

void set_ticks_counter(long l){
  ticks_counter = l;
  Serial.println("Counter: " + String(ticks_counter));
}


// POWER CONSUMPTION PART

long calculate_consumtion(long ms){
  long watt = 0;
  long time_passed = ms - lastPulse;

  // if pulse frequency is too high, something went wrong
  if(time_passed > THREHOLD_MS_POWER){
    Serial.println("time passed:" + String(time_passed,DEC));

    ticks_counter = ticks_counter + 1;
    // calcualte watt
    watt = calculate_watt(time_passed);

    last_watt = (double) watt;
    last_impulse_duration = time_passed;

    lastPulse = ms;
  } else {
    // in this case return -1 as the calculation will be wrong
    watt = -1;
  }
  return watt;
}

long calculate_watt(long time_passed){
  return (3600000 / time_passed) * WATT_HOURS_PER_IMPULSE;
}

void transmit_power_consumption(long watt){
  if(watt >= 0){
    if (!client.connected()) {
      long now = millis();
      if (now - lastReconnectAttempt > 5000) {
        lastReconnectAttempt = now;
        // Attempt to reconnect
        Serial.print("INFO: Attempting MQTT connection...");
        if (reconnect()) {
          lastReconnectAttempt = 0;
          publishData(watt);
          unsent_last_watt = -1;
        } else {
          unsent_last_watt = watt;
        }
      }
    } else {
    // Client connected
      publishData(watt);
      unsent_last_watt = -1;
    }
    display.showNumberDec((int) watt, true, 4,0);
  }
}

void pseudo_realtime_update(long now){
  long ms_since_last_pulse = now - lastPulse;

  if((ms_since_last_pulse > last_impulse_duration) && (last_watt > 0)){

    double estimated_watt = calculate_watt(ms_since_last_pulse);

    display.showNumberDec((int) estimated_watt, true, 4,0);
  }
}

// SETUP AND LOOP

void init_wifi(){
  WiFi.mode(WIFI_STA);
  Serial.println(WIFI_SSID);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("INFO: WiFi connected");
  Serial.println("INFO: IP address: ");
  Serial.println(WiFi.localIP());
}

void init_mqtt_connection(){
  // init the MQTT connection
  client.setServer(MQTT_SERVER_IP, MQTT_SERVER_PORT);
  client.setCallback(callback);
}

void inti_led_display() {
  display.setBrightness(1);
  display.showNumberDec((int) 0, true, 4,0);
}


void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  init_wifi();
  init_mqtt_connection();
  inti_led_display();
}


void do_the_magic(){
  long now = millis();
  int val = analogRead(lightsensor);

  pseudo_realtime_update(now);

  if(val > THREHOLD){
    transmit_power_consumption(calculate_consumtion(now));
  } else if(unsent_last_watt >= 0) {
    transmit_power_consumption(unsent_last_watt);
  }
}

void loop() {

  do_the_magic();

  if(client.connected()){
    client.loop();
  }
  delay(5);
}
